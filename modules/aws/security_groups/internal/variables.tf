variable "name" {
  type = string
}

variable "labels" {
  type = map(any)
}

variable "vpc_id" {
  type = string
}
