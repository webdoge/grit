variable "name" {
  type = string
}

variable "labels" {
  type = map(any)
}
