variable "url" {
  type = string
}

variable "gitlab_id" {
  type = string
}

variable "runner_description" {
  type = string
}

variable "runner_tags" {
  type = list(string)
}

variable "name" {
  type = string
}

variable "project_runner" {
  description = "Defining this as a project runner or not"
  type = bool
  default = true
}

variable "group_runner" {
  description = "Defining this as a group runner"
  type = bool
  default = false
}
