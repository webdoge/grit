resource "gitlab_user_runner" "primary" {
  description = "${var.runner_description} ${var.name}_GRIT"
  runner_type = var.project_runner ? "project_type" : "group_type"
  project_id  = var.project_runner ? var.gitlab_id : null
  group_id    = var.group_runner ? var.gitlab_id : null
  tag_list    = var.runner_tags
  untagged    = length(var.runner_tags) == 0 ? true : false
}

